package com.example.joyce.cadastrocurso;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        Bundle bundle2 = intent.getExtras();
        Bundle bundle3 = intent.getExtras();
        Bundle bundle4 = intent.getExtras();
        Bundle bundle5 = intent.getExtras();

        String txt = bundle.getString("txt");
        String txt2 = bundle2.getString("txt2");
        String txt3 = bundle3.getString("txt3");
        String txt4 = bundle4.getString("txt4");
        String txt5 = bundle5.getString("txt5");

        TextView textNome = (TextView) findViewById(R.id.txtNome2);
        textNome.setText("Nome: " + txt);
        TextView textDesc = (TextView) findViewById(R.id.txtDescricao2);
        textDesc.setText("Descrição: " + txt2);
        TextView textProf = (TextView) findViewById(R.id.txtProfessor2);
        textProf.setText("Professor: " + txt3);
        TextView textTip = (TextView) findViewById(R.id.txtTipo2);
        textTip.setText("Tipo: " + txt4);
        TextView textIn = (TextView) findViewById(R.id.txtInicio2);
        textIn.setText("Inicio: " + txt5);
    }
}
