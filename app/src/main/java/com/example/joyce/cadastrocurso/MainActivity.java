package com.example.joyce.cadastrocurso;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        Button btn = (Button) findViewById(R.id.btConfirmar);
        RadioGroup radioG = (RadioGroup) findViewById(R.id.radioGroup);
        final RadioButton rb1 = (RadioButton) findViewById(R.id.rbHumanas);
        final RadioButton rb2 = (RadioButton) findViewById(R.id.rbExatas);
        final RadioButton rb3 = (RadioButton) findViewById(R.id.rbTecnologico);
        final Spinner spinner = (Spinner) findViewById(R.id.spinner);

        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Activity2.class);

                String value = (String) spinner.getSelectedItem();
                Bundle bundle5 = new Bundle();
                bundle5.putString("txt5", value);
                intent.putExtras(bundle5);


                if(rb1.isChecked()){
                    TextView rbH = (TextView) findViewById(R.id.rbHumanas);
                    String txt4 = "Humanas";
                    txt4 = rbH.getText().toString().trim();
                    Bundle bundle4 = new Bundle();
                    bundle4.putString("txt4", txt4);
                    intent.putExtras(bundle4);
                }
                else if(rb2.isChecked()){
                    TextView rbE = (TextView) findViewById(R.id.rbExatas);
                    String txt4 = "Exatas";
                    txt4 = rbE.getText().toString().trim();
                    Bundle bundle4 = new Bundle();
                    bundle4.putString("txt4", txt4);
                    intent.putExtras(bundle4);
                }
                else if(rb3.isChecked()){
                    TextView rbT = (TextView) findViewById(R.id.rbTecnologico);
                    String txt4 = "Tecnólog";
                    txt4 = rbT.getText().toString().trim();
                    Bundle bundle4 = new Bundle();
                    bundle4.putString("txt4", txt4);
                    intent.putExtras(bundle4);
                }

                TextView edtNome = (TextView) findViewById(R.id.editNome);
                TextView edtDescricao = (TextView) findViewById(R.id.editDescricao);
                TextView edtProfessor = (TextView) findViewById(R.id.editProfessor);

                String txt = "";
                txt = edtNome.getText().toString();
                Bundle bundle = new Bundle();

                String txt2 = "";
                txt2 = edtDescricao.getText().toString();
                Bundle bundle2 = new Bundle();

                String txt3 = "";
                txt3 = edtProfessor.getText().toString();
                Bundle bundle3 = new Bundle();

                bundle.putString("txt", txt);
                intent.putExtras(bundle);

                bundle2.putString("txt2", txt2);
                intent.putExtras(bundle2);

                bundle3.putString("txt3", txt3);
                intent.putExtras(bundle3);

                //intent.putExtras(bundle);
                startActivity(intent);

                Toast.makeText(getApplicationContext(), "Fui clicado", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
